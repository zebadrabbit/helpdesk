'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('../app').mongoose;

var User = require('../models/modelUser');
var Message = require('../models/modelMessage');


router.get('/', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id, messageRead: false})
	.exec(function(err, messages) {
		if (err) throw err;
		User.find({}, function(err, supports) {
			res.render('message.html', {user: req.user, supports: supports, messages: messages});
		});
	});
});

router.post('/send', isLoggedIn, function (req, res) {
	
	var message = new Message(req.body);
	message.support = req.user;
	message.messageDate = new Date().getTime();
	
	message.save(function(err) {
		if (err) throw err;
		
		res.redirect('/message');
	});
	
});

router.get('/read', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id}, function(err, messages) {
		res.render('messageRead.html', {
			user: req.user,
			messages: messages
		});
	});

});

router.get('/done/:id', isLoggedIn, function (req, res) {

	Message.findOne({_id: req.params.id}, function(err, message) {
		message.messageRead = true;
		message.save(function(err) {
			if (err) throw err;
			
			res.redirect('/');
		});
	});

});


/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;

