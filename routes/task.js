'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('../app').mongoose;

var Task = require('../models/modelTask');
var Message = require('../models/modelMessage');
var Ticket = require('../models/modelTicket');
var Move = require('../models/modelMove');

router.get('/', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id, messageRead: false})
	.exec(function(err, messages) {
		
		Ticket.find({
			$and: [
				{issueStatus: 'Open'}, 
				{$or: [
					{issueAssignedTo: req.user._id}, 
					{issueAssignedTo: '0'}
				]}
			]
					
		})
		.exec(function(err, tickets) {
			
			Move.find({
				$and: [
					{moveStatus: 'Open'}, 
					{$or: [
						{moveAssignedTo: req.user.id}, 
						{moveAssignedTo: '0'}
					]}
				]
			})
			.exec(function(err, moves) {
				res.render('task.html', {user: req.user, tickets: tickets, moves: moves, messages: messages});
			});
		});
	});
});

router.post('/add', isLoggedIn, function (req, res) {
	
	req.body.infoDate = Date();
	
	var info = new Info(req.body);
	info.save(function(err) {
		if (err) throw err;
		
		res.redirect('/task');
	});
	
	
});

/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;
