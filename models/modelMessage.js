var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({

	to: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	support: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	messageDate: {
		type: Number,
		default: Date.now
	},
	messageTopic: String,
	messageContactName: String,
	messageContactEmail: String,
	messageContactPhone: String,
	messageContent: String,
	messageRead: {
		type: Boolean,
		default: false
	}
});

var Message = mongoose.model('Message', messageSchema);

module.exports = Message;
