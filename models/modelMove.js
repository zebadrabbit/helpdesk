var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var moveSchema = new Schema({

	moveEquipmentType: String,
	moveFrom: String,
	moveTo: String,
	moveRequestBy: String,
	moveStatus: String,
	moveRequiredBy: Number,
	moveAssignedTo: String,
	moveStatus: String,
	
	moveUpdates: [{

		moveUpdatedAt: {
			type: Number,
			default: Date.getTime
		},
		moveDetails: String,
		moveStatus: String,
		moveImportance: String,		
		support: {
			type: Schema.ObjectId,
			ref: 'User'
		}
	
	}]
	
});

var Move = mongoose.model('Move', moveSchema);

module.exports = Move;
