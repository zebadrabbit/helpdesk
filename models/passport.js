var mongoose = require('../app').mongoose;
var passport = require('../app').passport;
var LocalStrategy = require('passport-local').Strategy;

var User = require('./modelUser');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
	
    passport.use('local-signup', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            process.nextTick(function() {
                User.findOne({
                    'username': username
                }, function(err, user) {
					var newUser = new User()

					newUser.username = username;
					newUser.password = newUser.generateHash(password);
					newUser.email = req.body.email;

					newUser.save(function(err) {
						if (err) throw err;
						return done(null, newUser);
					});
                });
            });
        }
    ));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            process.nextTick(function() {
                User.findOne({
                    'username': username
                }, function(err, user) {
                    if (err) throw err;
                    if (!user)
                        return done(null, false, req.flash('loginErrorUsername', "No user found"));
                    if (!user.compareHash(password)) {
                        return done(null, false, req.flash('loginErrorPassword', "Wrong password"));
                    }
                    return done(null, user);
                });
            });
        }
    ));
};

function randomString(length) {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
