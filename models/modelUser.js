var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var userScehma = mongoose.Schema({
    username: String,
    password: String,
    email: String,
});


userScehma.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}

userScehma.methods.compareHash = function(password) {
  return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('User', userScehma);
