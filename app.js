﻿//
//	Helpdesk
//	elukens@nwacc.edu
//
//

'use strict';
var helmet = require('helmet');
var compression = require('compression')
var debug = require('debug');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var nunjucks = require('nunjucks');
var ntlm = require('express-ntlm');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
var bluebird = require('bluebird');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var moment = require('moment');

var app = express();

// view engine setup
var env = nunjucks.configure('views', {
	watch: true,
    autoescape: true,
    express: app
});

env.addFilter('unix', function(millis) {
    return moment(millis).format("MM/DD/YY hh:mm:ss a");
});

env.addFilter('getUser', function(id) {
	if (id == '0') {
		return 'First Available';
	} else {
		return id;
	}
});

app.set('view engine', 'nunjucks');

mongoose.Promise = bluebird;
mongoose.connect('mongodb://192.168.1.50/helpdesk', { 
	useMongoClient: true 
});

const store = new MongoDBStore({
    uri: 'mongodb://192.168.1.50/helpdesk',
    collection: 'sessions'
});

// Catch errors
store.on('error', function(error) {
    assert.ifError(error);
    assert.ok(false);
});


//app.use(ntlm({

//    debug: function () {
//        var args = Array.prototype.slice.apply(arguments);
//        console.log.apply(null, args);
//    },

//    domain: 'LUMPY',
//    domaincontroller: 'ldap://lumpy.local',

//}));

app.use(helmet());
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator())
app.use(cookieParser());

app.use(session({
	secret: 'oi4f6g5k0y5uwyx0051he5895', // change this!
    resave: true,
    saveUninitialized: true,
	store: store,
	cookie: { 
		maxAge: 1000 * 60 * 60 * 24 * 7
	}
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(express.static(path.join(__dirname, 'public')));

require('./models/passport')(passport);
module.exports.passport = passport;

app.use('/', require('./routes/index'));
app.use('/issue', require('./routes/issue'));
app.use('/message', require('./routes/message'));
app.use('/move', require('./routes/move'));
app.use('/task', require('./routes/task'));
app.use('/config', require('./routes/config'));
app.use('/login', require('./routes/login'));

app.use('/logout', function(req, res, next) {
    req.logout()
    res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error.html', {
            message: err.message,
            error: err
        });
		console.log(err);
		
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error.html', {
        message: err.message,
        error: err
    });
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});

module.exports = app;
